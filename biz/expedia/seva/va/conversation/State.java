package biz.expedia.seva.va.conversation;

import java.util.Map;

public interface State {
    StateResult performAction(Map context);
}
