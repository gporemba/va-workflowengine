package biz.expedia.seva.va.conversation.actions;

import biz.expedia.seva.va.conversation.Action;
import biz.expedia.seva.va.conversation.ActionResponse;

import java.util.Map;

public class GenericAction implements Action {

    public GenericAction() {}

    public ActionResponse performAction(Map<String, String> params) {
        System.out.println("The GenericAction is being Executed");

        return new ActionResponse() {

            public String getMessage() {
                return "This is a generic Step";
            }

            public String getResponse() { return null; }
        };
    }
}
