package biz.expedia.seva.va.conversation.actions;

import biz.expedia.seva.va.conversation.ActionResponse;
import biz.expedia.seva.va.conversation.Action;

import java.util.Map;

public class WaitAction implements Action {
    // Actions should be Singletons if possible

    public WaitAction() {}

    public ActionResponse performAction(Map<String, String> params) {
        return new ActionResponse() {

            public String getMessage() {
                return "Wait, do not execute the Next Step";
            }

            public String getResponse() { return null; }
        };
    }
}