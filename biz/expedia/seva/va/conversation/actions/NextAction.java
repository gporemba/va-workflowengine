package biz.expedia.seva.va.conversation.actions;

import biz.expedia.seva.va.conversation.Action;
import biz.expedia.seva.va.conversation.ActionResponse;

import java.util.Map;

public class NextAction implements Action {
    // Make this a Singleton

    public NextAction() {}

    public ActionResponse performAction(Map<String, String> params) {
        return new ActionResponse() {

            public String getMessage() {
                return "Execute the Next Step";
            }

            public String getResponse() { return null; }
        };
    }
}
