package biz.expedia.seva.va.conversation;

public interface ActionResponse {
    String getMessage();
    Object getResponse();
}
