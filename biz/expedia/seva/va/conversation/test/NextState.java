package biz.expedia.seva.va.conversation.test;

import biz.expedia.seva.va.conversation.State;
import biz.expedia.seva.va.conversation.StateResult;
import biz.expedia.seva.va.conversation.actions.GenericAction;
import biz.expedia.seva.va.conversation.Action;
import biz.expedia.seva.va.conversation.ActionResponse;

import java.util.Map;

public class NextState implements State {

    protected Action action;
    protected String nextStateName;

    public NextState(String nextStateName) {
        this.nextStateName = nextStateName;
        action = new GenericAction();
    }

    public StateResult performAction(Map context) {
        System.out.println("This is a NextState");
        ActionResponse result = action.performAction(null);
        System.out.println("The next state is: " + nextStateName);
        System.out.println();

        return new StateResult() {
            public Status getStatus() { return Status.NEXT;}
            public String getNextStateName() {return nextStateName;}
        };
    }
}
