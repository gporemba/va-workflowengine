package biz.expedia.seva.va.conversation.test;

import biz.expedia.seva.va.conversation.Action;
import biz.expedia.seva.va.conversation.ActionResponse;
import biz.expedia.seva.va.conversation.State;
import biz.expedia.seva.va.conversation.StateResult;
import biz.expedia.seva.va.conversation.actions.GenericAction;

import java.util.Map;

public class WaitState implements State {

    protected Action action;
    protected String nextStateName;

    public WaitState(String nextStateName) {
        this.nextStateName = nextStateName;
        action = new GenericAction();
    }

    public StateResult performAction(Map context) {
        System.out.println("This is a WaitState");
        ActionResponse result = action.performAction(null);
        System.out.println("The next state is: " + nextStateName);
        System.out.println();

        return new StateResult() {
            public Status getStatus() {
                return Status.WAIT;
            }

            public String getNextStateName() {
                return nextStateName;
            }
        };
    }
}
