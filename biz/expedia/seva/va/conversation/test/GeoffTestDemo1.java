package biz.expedia.seva.va.conversation.test;

import biz.expedia.seva.va.conversation.*;
import biz.expedia.seva.va.conversation.actions.NextAction;
import biz.expedia.seva.va.conversation.actions.WaitAction;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class GeoffTestDemo1 {
    public static void main(String[] args) {
        try {
            FlowCache cache = new FlowCache();
            FlowOrchestrator co = new FlowOrchestrator(cache);
            URI responseURI;

            Map flow = createCarCancel();
            cache.putFlow(new URI("//geoff/cancel#cancelcar"), flow);

            flow = createCancelHotel();
            cache.putFlow(new URI("//geoff/cancel#cancelhotel"), flow);

            flow = createCancelAir();
            cache.putFlow(new URI("//expedia/cancel#cancelair"), flow);

            responseURI = co.execute(new URI("//geoff/cancel#cancelcar"), new HashMap());
            co.execute(responseURI, new HashMap());

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }

    protected static Map createCancelHotel() {
        State state;
        Action nextAction = new NextAction();
        Action waitAction = new WaitAction();

        Map flow = new HashMap();
        Map params = new HashMap();

        params.put("message", "this is cancelHotel");
        state = new SimpleStateImpl(nextAction, params, "step2", StateResult.Status.NEXT);
        flow.put("cancelhotel", state);

        params = new HashMap();
        params.put("message", "this is step2");
        state = new SimpleStateImpl(nextAction, params, "step3", StateResult.Status.NEXT);
        flow.put("step2", state);

        params = new HashMap();
        params.put("message", "this is step3");
        state = new SimpleStateImpl(nextAction, params, "//expedia/cancel#cancelair", StateResult.Status.NEXT);
        flow.put("step3", state);

        return flow;
    }

    protected static Map createCancelAir() {
        State state;
        Action nextAction = new NextAction();
        Action waitAction = new WaitAction();

        Map flow = new HashMap();
        Map params = new HashMap();

        params.put("message", "this is cancelAir");
        state = new SimpleStateImpl(nextAction, params, "step2", StateResult.Status.NEXT);
        flow.put("cancelair", state);

        params = new HashMap();
        params.put("message", "this is step2");
        state = new SimpleStateImpl(nextAction, params, "step3", StateResult.Status.NEXT);
        flow.put("step2", state);

        params = new HashMap();
        params.put("message", "this is step3");
        state = new SimpleStateImpl(waitAction, params, "//geoff/cancel#cancelhotel", StateResult.Status.WAIT);
        flow.put("step3", state);

        return flow;
    }

    protected static Map createCarCancel() {
        State state;
        Action nextAction = new NextAction();
        Action waitAction = new WaitAction();

        Map flow = new HashMap();
        Map params = new HashMap();

        params.put("message", "this is cancelCar");
        state = new SimpleStateImpl(nextAction, params, "step2", StateResult.Status.NEXT);
        flow.put("cancelcar", state);

        params = new HashMap();
        params.put("message", "this is step2");
        state = new SimpleStateImpl(nextAction, params, "step3", StateResult.Status.NEXT);
        flow.put("step2", state);

        params = new HashMap();
        params.put("message", "this is step3");
        state = new SimpleStateImpl(waitAction, params, "//geoff/cancel#cancelhotel", StateResult.Status.WAIT);
        flow.put("step3", state);

        return flow;
    }
}
