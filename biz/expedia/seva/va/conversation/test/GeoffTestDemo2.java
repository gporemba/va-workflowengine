package biz.expedia.seva.va.conversation.test;

import biz.expedia.seva.va.conversation.Action;
import biz.expedia.seva.va.conversation.FlowCache;
import biz.expedia.seva.va.conversation.FlowOrchestrator;
import biz.expedia.seva.va.conversation.State;
import biz.expedia.seva.va.conversation.actions.NextAction;
import biz.expedia.seva.va.conversation.actions.WaitAction;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class GeoffTestDemo2 {

    public static void main(String[] args) {
        try {
            FlowCache cache = new FlowCache();
            FlowOrchestrator co = new FlowOrchestrator(cache);
            URI responseURI;

            Map flow = createCarCancel();
            cache.putFlow(new URI("//geoff/cancel#cancelcar"), flow);

            flow = createCancelHotel();
            cache.putFlow(new URI("//geoff/cancel#cancelhotel"), flow);

            flow = createCancelAir();
            cache.putFlow(new URI("//expedia/cancel#cancelair"), flow);

            responseURI = co.execute(new URI("//geoff/cancel#cancelcar"), new HashMap());
            co.execute(responseURI, new HashMap());

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }

    protected static Map createCancelHotel() {
        State state;

        Map flow = new HashMap();

        state = new NextState("step2");
        flow.put("cancelhotel", state);

        state = new NextState("step3");
        flow.put("step2", state);

        state = new NextState("//expedia/cancel#cancelair");
        flow.put("step3", state);

        return flow;
    }

    protected static Map createCancelAir() {
        State state;
        Map flow = new HashMap();

        state = new NextState( "step2");
        flow.put("cancelair", state);

        state = new NextState("step3");
        flow.put("step2", state);

        state = new WaitState("//geoff/cancel#cancelhotel");
        flow.put("step3", state);

        return flow;
    }

    protected static Map createCarCancel() {
        State state;
        Map flow = new HashMap();

        state = new NextState("step2");
        flow.put("cancelcar", state);

        state = new NextState("step3");
        flow.put("step2", state);

        state = new WaitState( "//geoff/cancel#cancelhotel");
        flow.put("step3", state);

        return flow;
    }
}
