package biz.expedia.seva.va.conversation.test;

import biz.expedia.seva.va.conversation.Action;
import biz.expedia.seva.va.conversation.State;
import biz.expedia.seva.va.conversation.ActionResponse;
import biz.expedia.seva.va.conversation.StateResult;


import java.util.Map;

public class SimpleStateImpl implements State {

    Action action;
    Map params;
    String nextStateName;
    StateResult.Status status;

    public SimpleStateImpl(Action action, Map params, String nextStateName, StateResult.Status status) {
        this.action = action;
        this.params = params;
        this.nextStateName = nextStateName;
        this.status = status;
    }

    public StateResult performAction(Map context) {
        ActionResponse response = action.performAction(getParams());
        System.out.println("The action is: " + action.getClass().getSimpleName());
        System.out.println("The params are: " + params);
        System.out.println("The response is: " + response.getResponse());
        System.out.println("The message is: " + response.getMessage());
        System.out.println();
        return createFlowResult(getNextStateName());

    }

    public Action getAction() {
        return action;
    }

    public Map getParams() {
        return params;
    }

    public String getNextStateName(){
        return nextStateName;
    }

    protected StateResult createFlowResult (String nextStateName) {
        return new StateResult(){
            public Status getStatus() {
                return status;
            }
            public String getNextStateName() {
                return nextStateName;
            }
        };
    }
}
