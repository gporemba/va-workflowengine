package biz.expedia.seva.va.conversation;

public interface StateResult {
    public enum Status {
        WAIT,
        NEXT,
        ERROR
    }
    Status getStatus();
    String getNextStateName();
}
