package biz.expedia.seva.va.conversation;

import java.util.Map;

public interface Action {
    ActionResponse performAction(Map<String, String> params);
}
