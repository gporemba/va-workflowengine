package biz.expedia.seva.va.conversation;

import java.util.Map;
import java.net.URI;

public class FlowOrchestrator {

    private static FlowCache flowcache;

    public FlowOrchestrator() {
        flowcache = new FlowCache();
    }

    public FlowOrchestrator(FlowCache flows) {
        flowcache = flows;
    }

    public URI execute (URI flowId, Map flow, Map context) {
        flowcache.putFlow(flowId, flow);
        return execute(flowId, context);
    }

    public URI execute(URI flowId, Map context) {
        Map<String, Object> flow = null;
        String nextState;
        //String step = null;
        StateResult result;
        State state;

        try {
            StateResult.Status status = StateResult.Status.NEXT;

            // load flow from cacheManager
            flow = flowcache.getFlow(flowId);
            nextState = flowId.getFragment();


            // Stage 3: Iterate over the flow until a wait state
            while (status == StateResult.Status.NEXT) {
                state = (State) flow.get(nextState);
                result = state.performAction(context);
                status = result.getStatus();

                nextState = result.getNextStateName();

                if (nextState.contains("/") == true) {
                    // need to load a new flow
                    flowId = new URI(nextState);
                    flow = flowcache.getFlow(flowId);
                    nextState = flowId.getFragment();
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return flowId;
    }
}
