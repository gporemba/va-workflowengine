package biz.expedia.seva.va.conversation;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.net.URI;

public class FlowCache {

    protected Map<URI, Map<String, Object>> cache;

    public FlowCache() {
        cache = new ConcurrentHashMap<URI,Map<String,Object>>();
    }

    public Map getFlow(URI flowId) {
        // check the cache
        Map flow = cache.get(flowId);

        if (flow == null) {
            /* load flow from DB
                or
                load class based on core namespace / tenantid / flowid

                for example, if the core namespace is biz.expedia.seva.va.conversation
                and the tenantid is "hotelsdotcom"
                and the flowid is "LodgingCancel"
                then the instantiation would look like:

                flow = Class.newInstance(biz.expedia.seva.va.conversation.hotelsdotcom.LodgingCancel);
             */
            //flow = (Map) Class.newInstance("biz.expedia.seva.va.conversation.hotelsdotcom.LodgingCancel");
            try {
                flow = (Map) Class.forName("biz.expedia.seva.va.conversation.hotelsdotcom.LodgingCancel").newInstance();
            } catch (Exception e) {

            }
        }

        return flow;
    }

    public void putFlow(URI flowId, Map flow) {
        cache.put(flowId, flow);
    }
}
